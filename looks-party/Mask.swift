//
//  Mask.swift
//  looks-party
//
//  Created by BEPID on 26/07/16.
//  Copyright © 2016 BEPID. All rights reserved.
//

import UIKit
import ObjectMapper

class Mask: NSObject,Mappable {

    
    var id : String?
    var thumbUrl : String?
    var maskUrl : String?
    
    convenience required init?(_ map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        thumbUrl <- map["thumbUrl"]
        maskUrl <- map["maskUrl"]
    }
}
