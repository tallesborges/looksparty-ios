//
//  PublicationTableViewCell.swift
//  looks-party
//
//  Created by BEPID on 27/07/16.
//  Copyright © 2016 BEPID. All rights reserved.
//

import UIKit
import AlamofireImage

class PublicationTableViewCell: UITableViewCell {
    
    @IBOutlet weak var publicationText: UILabel!
    @IBOutlet weak var publicationImageView: UIImageView!
    
    var publication : Publication?{
        didSet{
            update()
        }
    }
    
    func update(){
        let color = publication?.api_color()
        
        self.backgroundColor = color
        publicationImageView.backgroundColor = color
        self.contentView.backgroundColor = color
        publicationText.text = publication?.text
        
        if let url = publication?.api_imageUrl() {
            publicationImageView.af_setImageWithURL(url)
        }else{
            publicationImageView.image = nil
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
