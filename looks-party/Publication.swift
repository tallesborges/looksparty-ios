//
//  Publication.swift
//  looks-party
//
//  Created by BEPID on 26/07/16.
//  Copyright © 2016 BEPID. All rights reserved.
//

import UIKit
import ObjectMapper

class Publication: NSObject, Mappable {

    var id : String?
    var imageUrl : String?
    var text : String?
    var colorR : Float = 0
    var colorG : Float = 0
    var colorB : Float = 0
    
    convenience required init?(_ map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        imageUrl <- map["imageUrl"]
        text <- map["text"]
        colorR <- map["colorR"]
        colorG <- map["colorG"]
        colorB <- map["colorB"]
    }
    
}

extension Publication {    
    func api_imageUrl() -> NSURL? {
        
        if let imageUrl = imageUrl {
            return NSURL(string: API.baseUrl + imageUrl)
        }
        
        return nil
    }
    
    func api_color() -> UIColor {
        let red = CGFloat(colorR)
        let green = CGFloat(colorG)
        let blue = CGFloat(colorB)
        return UIColor(red: red, green: green, blue: blue, alpha: 1)
    }
}
