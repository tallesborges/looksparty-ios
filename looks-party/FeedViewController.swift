//
//  FeedViewController.swift
//  looks-party
//
//  Created by BEPID on 19/07/16.
//  Copyright © 2016 BEPID. All rights reserved.
//

import UIKit

class FeedViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var refreshControl : UIRefreshControl!
    
    var publications = [Publication](){
        didSet{
            self.tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tableView.tableFooterView = UIView()
        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(self.refresh(_:)), forControlEvents: .ValueChanged)
        tableView.backgroundView = refreshControl
    }
    
    func refresh(refreshControl: UIRefreshControl) {
        updateData()
    }

    override func viewWillAppear(animated: Bool) {
        updateData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}


extension FeedViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: false)
        
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("commentsVC") as! CommentsViewController
        vc.publication = publications[indexPath.row]
        
        self.presentViewController(vc, animated: true, completion: nil)
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return publications.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("cell") as! PublicationTableViewCell
        cell.publication = publications[indexPath.row]
        
        return cell
    }
    
}

extension FeedViewController {
    
    func updateData(){
        self.refreshControl.beginRefreshing()
        API.Publications.get { (result) in
            self.refreshControl.endRefreshing()
            switch(result){
            case .Success(let value):
                self.publications = value
                break
            case .Failure(let error):
                print(error)
                break
            }
        }
    }
}