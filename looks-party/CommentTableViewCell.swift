//
//  CommentTableViewCell.swift
//  looks-party
//
//  Created by BEPID on 27/07/16.
//  Copyright © 2016 BEPID. All rights reserved.
//

import UIKit
import DateTools

class CommentTableViewCell: UITableViewCell {

    @IBOutlet weak var commentText: UILabel!
    @IBOutlet weak var commentTime: UILabel!
    @IBOutlet weak var likeButton: UIButton!
    
    
    var comment : Comment? {
        didSet {
            update()
        }
    }
    
    func update(){
        
        if let comment = comment {
            commentText.text = comment.text
        }
        
        if let date = comment?.createdDate {
            commentTime.text = date.timeAgoSinceNow()
        }
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func likePressed(sender: AnyObject) {
        likeButton.selected = !likeButton.selected
    }
    
}
