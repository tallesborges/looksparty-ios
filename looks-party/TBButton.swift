//
//  TBButton.swift
//  looks-party
//
//  Created by BEPID on 02/08/16.
//  Copyright © 2016 BEPID. All rights reserved.
//

import UIKit

class TBButton: UIButton {

    @IBInspectable var borderColor : UIColor = UIColor.clearColor(){
        didSet{
            setup()
        }
    }
    
    @IBInspectable var borderWidth : CGFloat = 0{
        didSet{
            setup()
        }
    }
    @IBInspectable var cornerRadius : CGFloat = 0{
        didSet{
            setup()
        }
    }

    @IBInspectable var shadowColor : UIColor = UIColor.clearColor(){
        didSet{
            setup()
        }
    }
    @IBInspectable var shadowOpacity : Float = 0{
        didSet{
            setup()
        }
    }
    @IBInspectable var shadowOffset : CGSize = CGSizeMake(0, 0){
        didSet{
            setup()
        }
    }
    
    @IBInspectable var shadowRadius : CGFloat = 0{
        didSet{
            setup()
        }
    }
    
    
    func setup(){
        self.layer.borderColor = self.borderColor.CGColor
        self.layer.borderWidth = self.borderWidth
        self.layer.cornerRadius = self.cornerRadius
        self.layer.shadowColor = self.shadowColor.CGColor
        self.layer.shadowOpacity = self.shadowOpacity
        self.layer.shadowOffset = self.shadowOffset
        self.layer.shadowRadius = self.shadowRadius
    }
    
}


