//
//  NewPostViewController.swift
//  looks-party
//
//  Created by BEPID on 19/07/16.
//  Copyright © 2016 BEPID. All rights reserved.
//

import UIKit
import SVProgressHUD

class VerticallyCenteredTextView: UITextView {
    override var contentSize: CGSize {
        didSet {
            var topCorrection = (bounds.size.height - contentSize.height * zoomScale) / 2.0
            topCorrection = max(0, topCorrection)
            contentInset = UIEdgeInsets(top: topCorrection, left: 0, bottom: 0, right: 0)
        }
    }
    
}

class NewPostViewController: UIViewController {

    var colors = [
        UIColor(red:0.17, green:0.62, blue:0.52, alpha:1.00),
        UIColor(red:0.22, green:0.67, blue:0.40, alpha:1.00),
        UIColor(red:0.20, green:0.51, blue:0.71, alpha:1.00),
        UIColor(red:0.55, green:0.30, blue:0.66, alpha:1.00),
        UIColor(red:0.18, green:0.24, blue:0.31, alpha:1.00),
        UIColor(red:0.94, green:0.76, blue:0.25, alpha:1.00),
        UIColor(red:0.89, green:0.49, blue:0.23, alpha:1.00),
        UIColor(red:0.89, green:0.31, blue:0.28, alpha:1.00),
        UIColor(red:0.58, green:0.65, blue:0.65, alpha:1.00)
    ]
    
    var colorIndex = 0
    
    // MARK: - Outlets
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var selectedImageView: UIImageView!
    @IBOutlet weak var publicationView: UIView!
    
    var blurView : UIVisualEffectView?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        textView.becomeFirstResponder()
        updateBackground()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        textView.resignFirstResponder()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Actions
    @IBAction func addPhotoPressed(sender: AnyObject) {
        let imagePicker = UIImagePickerController()
        imagePicker.allowsEditing = true
        imagePicker.sourceType = .PhotoLibrary
        imagePicker.delegate = self
        textView.resignFirstResponder()
        self.presentViewController(imagePicker, animated: true, completion: nil)
    }

    func updateBackground(){
        if (colorIndex + 1 >= colors.count){
            colorIndex = 0
        }else{
            colorIndex += 1
        }
        self.publicationView.backgroundColor = colors[colorIndex]
    }
    
    @IBAction func changeColorPressed(sender: AnyObject) {
        updateBackground()
    }
    
    func getImageToSave() -> UIImage? {
        return selectedImageView.image
    }
    
    
    @IBAction func sendPressed(sender: AnyObject) {
        SVProgressHUD.show()
        if let image  = getImageToSave() {
            API.Upload.image(image, completionHandler: { (result) in
                if let imageUrl = result.value??["url"] as? String{
                    self.createPublication(imageUrl)
                }
            })
        }else{
            createPublication(nil)
        }
    }
    
    func createPublication(urlImage : String?){
        let publication = Publication()
        publication.imageUrl = urlImage
        publication.text = self.textView.text
        var red:   CGFloat = 0
        var green: CGFloat = 0
        var blue:  CGFloat = 0
        
        self.publicationView.backgroundColor?.getRed(&red, green: &green, blue: &blue, alpha: nil)
        
        publication.colorR = Float(red)
        publication.colorG = Float(green)
        publication.colorB = Float(blue)
        
        API.Publications.create(publication, completionHandler: { (result) in
            SVProgressHUD.dismiss()
            self.dismissViewControllerAnimated(true, completion: nil)
            
            switch(result){
            case .Success(let value) :
                print(value)
                break
            case .Failure(let error):
                print(error)
                break
            }
            
        })
    }
    
    @IBAction func closePressed(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
}

extension NewPostViewController : UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        print(info)
        if let image = info[UIImagePickerControllerEditedImage] as? UIImage {
            
            let inputImage = image
            let ciContext = CIContext(options: nil)
            
            let blurFilter = CIFilter(name: "CIGaussianBlur")
            blurFilter!.setValue(CIImage(image: inputImage), forKey: "inputImage")
            blurFilter!.setValue(50, forKey: "inputRadius")
            
            let outputImageData = blurFilter!.valueForKey("outputImage") as! CIImage
            
            var rect             = outputImageData.extent;
            rect.origin.x          += (rect.size.width  - image.size.width ) / 2;
            rect.origin.y          += (rect.size.height - image.size.height) / 2;
            rect.size               = image.size;
            
            let outputImageRef: CGImage = ciContext.createCGImage(outputImageData, fromRect: rect)
            
            let outputImage = UIImage(CGImage: outputImageRef)
            
            
            self.selectedImageView.image = outputImage
        }
        
        picker.dismissViewControllerAnimated(true, completion: nil)
        textView.becomeFirstResponder()
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        textView.becomeFirstResponder()
        picker.dismissViewControllerAnimated(true, completion: nil)
    }
}

extension NewPostViewController : UITextViewDelegate {
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if (text == "") {return true}
        return textView.contentSize.height < textView.bounds.size.height - 40
    }
}
