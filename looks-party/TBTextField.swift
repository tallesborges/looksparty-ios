//
//  TBTextField.swift
//  looks-party
//
//  Created by BEPID on 02/08/16.
//  Copyright © 2016 BEPID. All rights reserved.
//

import UIKit

class TBTextField: UITextField {

    // Margin
    @IBInspectable var top : CGFloat =  0
    @IBInspectable var left : CGFloat =  10
    @IBInspectable var bottom : CGFloat =  0
    @IBInspectable var right : CGFloat =  10
    
    override func textRectForBounds(bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, UIEdgeInsets(top: top, left: left, bottom: bottom, right: right))
    }
    
    override func placeholderRectForBounds(bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, UIEdgeInsets(top: top, left: left, bottom: bottom, right: right))
    }
    
    override func editingRectForBounds(bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, UIEdgeInsets(top: top, left: left, bottom: bottom, right: right))
    }
    
    
    // Layer
    @IBInspectable var borderColor : UIColor = UIColor.clearColor(){
        didSet{
            setup()
        }
    }
    
    @IBInspectable var borderWidth : CGFloat = 0{
        didSet{
            setup()
        }
    }
    @IBInspectable var cornerRadius : CGFloat = 0{
        didSet{
            setup()
        }
    }
    
    @IBInspectable var shadowColor : UIColor = UIColor.clearColor(){
        didSet{
            setup()
        }
    }
    @IBInspectable var shadowOpacity : Float = 0.0 {
        didSet{
            setup()
        }
    }
    @IBInspectable var shadowOffsetWidth : CGFloat = 0 {
        didSet{
            setup()
        }
    }
    
    @IBInspectable var shadowOffsetHeight : CGFloat = 0 {
        didSet{
            setup()
        }
    }
    
    @IBInspectable var shadowRadius : CGFloat = 0{
        didSet{
            setup()
        }
    }
    
    func setup(){
        self.layer.borderColor = self.borderColor.CGColor
        self.layer.borderWidth = self.borderWidth
        self.layer.cornerRadius = self.cornerRadius
        self.layer.shadowColor = self.shadowColor.CGColor
        self.layer.shadowOpacity = self.shadowOpacity
        self.layer.shadowOffset = CGSizeMake(shadowOffsetWidth, shadowOffsetHeight)
        self.layer.shadowRadius = self.shadowRadius
    }

}
