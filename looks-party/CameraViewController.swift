//
//  CameraViewController.swift
//  looks-party
//
//  Created by BEPID on 19/07/16.
//  Copyright © 2016 BEPID. All rights reserved.
//

import UIKit
import AVFoundation

class CameraView : UIView {
    
    override class func layerClass() -> AnyClass {
        return AVCaptureVideoPreviewLayer.self
    }
    
    var session : AVCaptureSession!{
        get {
            let layer = self.layer as! AVCaptureVideoPreviewLayer
            return layer.session
        }
        set {
            let layer = self.layer as! AVCaptureVideoPreviewLayer
            layer.videoGravity = AVLayerVideoGravityResizeAspectFill
            layer.masksToBounds = true
            layer.session = newValue
        }
    }
}


class CameraViewController: UIViewController {

    // Resource
    var masks = [Mask](){
        didSet{
            thumbMasksCollectionView.reloadData()
        }
    }
    
    
    // MARK: - Outlets
    @IBOutlet weak var takedPhotoImageView: UIImageView!
    @IBOutlet weak var maskImageView: UIImageView!
    @IBOutlet weak var thumbMasksCollectionView: UICollectionView!
    @IBOutlet weak var cameraView: CameraView!
    
    
    @IBOutlet weak var turnCameraButton: UIButton!
    @IBOutlet weak var flashButton: UIButton!
    
    @IBOutlet weak var galeryButton: UIButton!
    @IBOutlet weak var removeButton: UIButton!
    
    @IBOutlet weak var takePhotoButton: UIButton!
    @IBOutlet weak var shareButton: UIButton!
    
    var stillOutput : AVCaptureStillImageOutput!
    var captureSession : AVCaptureSession!
    var deviceInput : AVCaptureDeviceInput!
    var cameraPosition : AVCaptureDevicePosition = .Unspecified
    var actualDevice : AVCaptureDevice!
    
    var docController = UIDocumentInteractionController()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        API.Mask.list { (result) in
            if let result = result.value {
                self.masks = result
            }
        }
        
        initCamera()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(animated: Bool) {
        self.captureSession.stopRunning()
    }

    
    func initCamera(){
        captureSession =  AVCaptureSession()
        captureSession.sessionPreset = AVCaptureSessionPreset1280x720
        cameraView.session = captureSession
        
        
        captureSession.beginConfiguration()
        
        let devices = AVCaptureDevice.devicesWithMediaType(AVMediaTypeVideo)
        let device = devices.first as! AVCaptureDevice
        actualDevice = device
        
        flashButton.selected = device.flashMode != .Off
        
        cameraPosition = device.position
        
        deviceInput = try! AVCaptureDeviceInput(device: device)
        captureSession.addInput(deviceInput)
        
        stillOutput = AVCaptureStillImageOutput()
        captureSession.addOutput(stillOutput)
        
        self.captureSession.commitConfiguration()
        
        enterPhotoMode()
    }

    // MARK: - Actions
    
    @IBAction func openGaleryPressed(sender: AnyObject) {
        let imagePicker = UIImagePickerController()
        imagePicker.allowsEditing = true
        imagePicker.sourceType = .PhotoLibrary
        imagePicker.delegate = self
        self.presentViewController(imagePicker, animated: true, completion: nil)
    }
    
    @IBAction func photoPressed(sender: AnyObject) {
    
        let connection = stillOutput.connectionWithMediaType(AVMediaTypeVideo)
        
        stillOutput.captureStillImageAsynchronouslyFromConnection(connection) { (buffer, error) in
            let data = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(buffer)
            let image = UIImage(data: data)
            
            self.takedPhotoImageView.image = image
            
            self.enterShareMode()
        }
    }
    
    func enterPhotoMode(){
        self.takedPhotoImageView.hidden = true
        self.galeryButton.hidden = false
        self.removeButton.hidden = true
        
        self.takePhotoButton.hidden = false
        self.shareButton.hidden = true
        
        turnCameraButton.hidden = false
        flashButton.hidden = false
        
        self.captureSession.startRunning()
    }
    
    func enterShareMode(){
         self.takedPhotoImageView.hidden = false
        self.galeryButton.hidden = true
        self.removeButton.hidden = false
        
        turnCameraButton.hidden = true
        flashButton.hidden = true
        
        self.takePhotoButton.hidden = true
        self.shareButton.hidden = false
        
        self.captureSession.stopRunning()
    }
    
    @IBAction func flashPressed(sender: AnyObject) {
        flashButton.selected = !flashButton.selected
        
        
        try! actualDevice.lockForConfiguration()
        
        if (flashButton.selected){
            actualDevice.flashMode = .On
        }else {
            actualDevice.flashMode = .Off
        }
        
        actualDevice.unlockForConfiguration()
    }
    
    @IBAction func turnCameraPressed(sender: AnyObject) {
        let devices = AVCaptureDevice.devicesWithMediaType(AVMediaTypeVideo)
        for device in devices {
            let device = device as! AVCaptureDevice
            if device.position != cameraPosition {
                actualDevice = device
                
                cameraPosition = device.position
                
                captureSession.beginConfiguration()
                
                captureSession.removeInput(deviceInput)
                
                deviceInput = try! AVCaptureDeviceInput(device: device)
                captureSession.addInput(deviceInput)
                
                captureSession.commitConfiguration()
                break
            }
        }
       
        
    }
    
    @IBAction func closePressed(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func removePressed(sender: AnyObject) {
        enterPhotoMode()
    }
    
    @IBAction func sharePressed(sender: AnyObject) {
     
        let stringUrl = NSTemporaryDirectory().stringByAppendingString("image.jpg")
        let url = NSURL(fileURLWithPath:  stringUrl)
        
        UIImageJPEGRepresentation(self.takedPhotoImageView.image!, 1)?.writeToURL(url, atomically: true)
        
        self.docController = UIDocumentInteractionController(URL: url)
        self.docController.UTI = "com.instagram.photo"
        self.docController.delegate = self
        self.docController.annotation = ["InstagramCaption":"#looksparty"]
        
        self.docController.presentOptionsMenuFromRect(CGRectZero, inView: self.view, animated: true)
    }
    
}

extension CameraViewController : UIDocumentInteractionControllerDelegate {
    
}

extension CameraViewController : UICollectionViewDelegate, UICollectionViewDataSource{
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return masks.count
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        let mask = masks[indexPath.row]
        
        if let maskURL = api_URL(mask.maskUrl){
            maskImageView.af_setImageWithURL(maskURL)
        }
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("cell", forIndexPath: indexPath) as! ThumbCollectionViewCell
        
        cell.mask = masks[indexPath.row]
        
        return cell
    }
    
}

extension CameraViewController : UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        print(info)
        if let image = info[UIImagePickerControllerEditedImage] as? UIImage {
            self.takedPhotoImageView.image = image
            
            self.enterShareMode()
        }
        
        picker.dismissViewControllerAnimated(true, completion: nil)
        
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        picker.dismissViewControllerAnimated(true, completion: nil)
    }
}