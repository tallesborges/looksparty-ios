//
//  API.swift
//  NexuApp
//
//  Created by BEPID on 24/05/16.
//  Copyright © 2016 BEPID. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper
import ObjectMapper
import CoreLocation


enum NetworkConstants {
    static let baseApiUrl = "http://looksparty.tallesborges.com/api"
}


typealias CompletionBlockAPI = ((Result<AnyObject?, APIError>) -> ())?

struct APIError: ErrorType {
    var message: String?

    init(message: String?) {
        self.message = message
    }

    init(nsError: NSError?) {
        self.message = "Não foi possivel efetuar esta operação"
    }

    init(data: NSData?) {

        guard data != nil else {
            self.message = "Não foi possivel efetuar esta operação"
            return
        }

        let resultData = try? NSJSONSerialization.JSONObjectWithData(data!, options: .AllowFragments) as? [String : AnyObject]

        if let errors = resultData??["errors"] as? [String] {
            self.message = errors[0] ?? "Não foi possivel efetuar esta operação"
        } else if let errors = resultData??["Errors"] as? [String] {
            self.message = errors[0] ?? "Não foi possivel efetuar esta operação"
        } else if let error = resultData??["error_description"] as? String {
            self.message = error
        } else {
            self.message = "Não foi possivel efetuar esta operação"
        }

    }
}

struct API {
    static var token: String? {
        didSet {
            if token != nil {
                UIApplication.sharedApplication().registerForRemoteNotifications()
            }
        }
    }

    static var baseUrl: String {
        get {
            return NetworkConstants.baseApiUrl
        }
    }

    static func requestDATA(method: Alamofire.Method, url: String, parameters: [String : AnyObject]?, encoding: ParameterEncoding = .JSON, expectedCode: Int = 200, auth: Bool = true, completionHandler: CompletionBlockAPI) {
        let token = auth ? ["Authorization" : API.token ?? ""] : [:]

        Alamofire.request(method, url, parameters: parameters, encoding: encoding, headers: token).response { response in
            print(response.0)
            print(response.1)

            if let error = response.3 {
                let result = Result<AnyObject?, APIError>.Failure(APIError(nsError: error))
                completionHandler?(result)
                return
            }


            guard response.1?.statusCode == expectedCode else {
                let result = Result<AnyObject?, APIError>.Failure(APIError(data: response.2))
                completionHandler?(result)
                return
            }



            let result = Result<AnyObject?, APIError>.Success(response.2)
            completionHandler?(result)

        }
    }

    static func requestJSON(method: Alamofire.Method, url: String, parameters: [String : AnyObject]?, encoding: ParameterEncoding = .JSON, expectedCode: Int = 200, auth: Bool = true, completionHandler: CompletionBlockAPI) {

        let token = auth ? ["Authorization" : API.token ?? ""] : [:]

        Alamofire.request(method, url, parameters: parameters, encoding: encoding, headers: token).responseJSON { response in
            print(response.request)
            print(response.response)
            switch response.result {
            case .Success(let value):

                guard response.response?.statusCode == expectedCode else {
                    let result = Result<AnyObject?, APIError>.Failure(APIError(data: response.data))
                    completionHandler?(result)
                    return
                }

                let result = Result<AnyObject?, APIError>.Success(value)
                completionHandler?(result)
                break
            case .Failure(let error):
                let result = Result<AnyObject?, APIError>.Failure(APIError(nsError: error))
                completionHandler?(result)
                break
            }
        }
    }

    static func requestObject<T: Mappable>(method: Alamofire.Method, url: String, parameters: [String : AnyObject]?, encoding: ParameterEncoding = .JSON, expectedCode: Int = 200, auth: Bool = true, completionHandler: ((Result<T, APIError>) -> ())?) {

        let token = auth ? ["Authorization" : API.token ?? ""] : [:]

        Alamofire.request(method, url, parameters: parameters, encoding: encoding, headers: token).responseObject { (response: Response<T, NSError>) in
            print(response.request)
            print(response.response)
            switch response.result {
            case .Success(let value):
                guard response.response?.statusCode == expectedCode else {
                    let result = Result<T, APIError>.Failure(APIError(data: response.data))
                    completionHandler?(result)
                    return
                }

                let result = Result<T, APIError>.Success(value)
                completionHandler?(result)
            case .Failure(let error):
                let result = Result<T, APIError>.Failure(APIError(nsError: error))
                completionHandler?(result)
            }
        }
    }

    static func requestArray<T: Mappable>(method: Alamofire.Method, url: String, parameters: [String : AnyObject]?, encoding: ParameterEncoding = .JSON, expectedCode: Int = 200, auth: Bool = true, completionHandler: ((Result<[T], APIError>) -> ())?) {

        let token = auth ? ["Authorization" : API.token ?? ""] : [:]

        Alamofire.request(method, url, parameters: parameters, encoding: encoding, headers: token).responseArray { (response: Response<[T], NSError>) in
            print(response.request)
            print(response.response)
            switch response.result {
            case .Success(let value):

                guard response.response?.statusCode == expectedCode else {
                    let result = Result<[T], APIError>.Failure(APIError(data: response.data))
                    completionHandler?(result)
                    return
                }

                let result = Result<[T], APIError>.Success(value)
                completionHandler?(result)
                break
            case .Failure(let error):
                let result = Result<[T], APIError>.Failure(APIError(nsError: error))
                completionHandler?(result)
                break
            }
        }
    }

    static func requestUploadObject<T: Mappable>(method: Alamofire.Method, url: String, imageData: NSData, completionHandler: ((Result<T, APIError>) -> ())?) {

        Alamofire.upload(method, url, headers: nil, multipartFormData: { (multiform) -> Void in
            multiform.appendBodyPart(data: imageData, name: "file", fileName: "file.jpg", mimeType: "image/jpeg")
        }) { (result) -> Void in

            print(result)
            switch result {
            case .Success(let upload, _, _):
                upload.responseObject(completionHandler: { (response: Response<T, NSError>) in
                    switch response.result {
                    case .Success(let value):
                        let result = Result<T, APIError>.Success(value)
                        completionHandler?(result)
                        break
                    case .Failure(let error):
                        let result = Result<T, APIError>.Failure(APIError(nsError: error))
                        completionHandler?(result)
                        break
                    }
                })
                break

            case .Failure(let error):
                let result = Result<T, APIError>.Failure(APIError(message: "Não foi possivel efetuar esta operação"))
                print(error)
                completionHandler?(result)
                break
            }
        }
    }
    
    static func requestUploadJSON(method: Alamofire.Method, url: String, imageData: NSData, completionHandler: CompletionBlockAPI) {
        
        Alamofire.upload(method, url, headers: nil, multipartFormData: { (multiform) -> Void in
            multiform.appendBodyPart(data: imageData, name: "file", fileName: "file.jpg", mimeType: "image/jpeg")
        }) { (result) -> Void in
            
            print(result)
            switch result {
            case .Success(let upload, _, _):
                
                upload.responseJSON(completionHandler: { (response) in
                    switch response.result {
                    case .Success(let value):
                        let result = Result<AnyObject?, APIError>.Success(value)
                        completionHandler?(result)
                        break
                    case .Failure(let error):
                        let result = Result<AnyObject?, APIError>.Failure(APIError(nsError: error))
                        completionHandler?(result)
                        break
                    }
                })

            case .Failure(let error):
                let result = Result<AnyObject?, APIError>.Failure(APIError(message: "Não foi possivel efetuar esta operação"))
                print(error)
                completionHandler?(result)
                break
            }
        }
        
    }
}
