//
//  ContainerViewController.swift
//  looks-party
//
//  Created by BEPID on 19/07/16.
//  Copyright © 2016 BEPID. All rights reserved.
//

import UIKit

class ContainerViewController: UIViewController {

    
    // MARK : Outlets

    @IBOutlet weak var titleImageView: UIImageView!
    @IBOutlet weak var beerButtonBar: UIButton!
    @IBOutlet weak var cameraButtonBar: UIButton!
    @IBOutlet weak var instagramButtonBar: UIButton!
    @IBOutlet weak var aboutButtonBar: UIButton!
    @IBOutlet weak var newPostButton: UIButton!
    @IBOutlet weak var containerView: UIView!
    
    weak var currentVC : UIViewController?
    weak var currentButton : UIButton?
    
    var feedVC : UIViewController!
    var instaVC : UIViewController!
    var aboutVC : UIViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        feedVC = self.storyboard!.instantiateViewControllerWithIdentifier("feedVC")
        instaVC = self.storyboard!.instantiateViewControllerWithIdentifier("instaVC")
        aboutVC = self.storyboard!.instantiateViewControllerWithIdentifier("aboutVC")
        
        selectButton(beerButtonBar)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func openFeed(){
        self.changeFrom(currentVC, to: feedVC)
    }
    
    func openInsta(){
        self.changeFrom(currentVC, to: instaVC)
    }
    
    func openAbout(){
        self.changeFrom(currentVC, to: aboutVC)
    }

    // MARK : Utils
    func changeFrom(oldVC : UIViewController?, to:UIViewController){
        
        if let oldVC = oldVC {
            oldVC.willMoveToParentViewController(nil)
            oldVC.view.removeFromSuperview()
            oldVC.removeFromParentViewController()
        }
        
        self.addChildViewController(to)
        
        to.view.frame = containerView.bounds
        self.containerView.addSubview(to.view)
        to.didMoveToParentViewController(self)
        
    }
    
    // MARK : Actions
    @IBAction func beerPressed(sender: UIButton) {
        selectButton(sender)
    }
    
    @IBAction func cameraPressed(sender: UIButton) {
        
    }
    
    @IBAction func instagramPressed(sender: UIButton) {
        selectButton(sender)
    }
    
    @IBAction func aboutPressed(sender: UIButton) {
        selectButton(sender)
    }
    
    // MARK : Tab Bar Utils
    func deselectAll(){
        beerButtonBar.selected = false
        cameraButtonBar.selected = false
        instagramButtonBar.selected = false
        aboutButtonBar.selected = false
        newPostButton.hidden = true
    }
    
    func selectButton(button : UIButton){
        
        guard currentButton != button else {
            return
        }
        
        deselectAll()
        
        switch button {
        case beerButtonBar:
            titleImageView.image = UIImage(named: "title_feed")
            newPostButton.hidden = false
            openFeed()
            break
        case cameraButtonBar:
            break
        case instagramButtonBar:
            titleImageView.image = UIImage(named: "title_instagram")
            openInsta()
            break
        case aboutButtonBar:
            titleImageView.image = UIImage(named: "title_about")
            openAbout()
            break
        default:
            print(button)
        }
        
        button.selected = true
    }
}
