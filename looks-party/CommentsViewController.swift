//
//  CommentsViewController.swift
//  looks-party
//
//  Created by BEPID on 20/07/16.
//  Copyright © 2016 BEPID. All rights reserved.
//

import UIKit
import AlamofireImage
import SVProgressHUD

class CommentsViewController: UIViewController {

    @IBOutlet weak var commentsTableView: UITableView!
    @IBOutlet weak var commentTextField: UITextField!
    @IBOutlet weak var publicationImageView: UIImageView!
    @IBOutlet weak var publicationText: UILabel!
    
    var refreshControl : UIRefreshControl!
    
    var publication : Publication?
    
    var comments = [Comment](){
        didSet{
            commentsTableView.reloadData()
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        if let urlImage = publication?.api_imageUrl(){
            publicationImageView.af_setImageWithURL(urlImage)
        }
        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(self.refresh(_:)), forControlEvents: .ValueChanged)
        commentsTableView.backgroundView = refreshControl
        
        publicationText.text = publication?.text
        publicationImageView.backgroundColor = publication?.api_color()
        self.view.backgroundColor = publication?.api_color()
        
        commentsTableView.tableFooterView = UIView()
        
        commentTextField.setNeedsLayout()
        
        updateComments()
    }
    
    func refresh(refreshControl: UIRefreshControl) {
        updateComments()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        commentTextField.resignFirstResponder()
    }
    
    func updateComments(){
        
        if let id = publication?.id {
            self.refreshControl.beginRefreshing()
            API.Publications.Comments.get(id) { (result) in
                self.refreshControl.endRefreshing()
                switch(result){
                case .Success(let value):
                    self.comments = value
                    break
                case .Failure(let error):
                    print(error)
                    break
                }
            }
        }
        
    }
    
    @IBAction func sentPressed(sender: AnyObject) {
        let comment = Comment()
        comment.text = commentTextField.text
        
        SVProgressHUD.show()
        if let publicationId = self.publication?.id {
            API.Publications.Comments.create(publicationId, comment: comment) { (result) in
                SVProgressHUD.dismiss()
                self.commentTextField.text = ""
                switch(result){
                case .Success(let value):
                    print(value)
                    self.updateComments()
                    break
                case .Failure(let error):
                    print(error)
                    break
                }
            }
        }
    }
    

    @IBAction func backPressed(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
}

extension CommentsViewController :UITableViewDataSource {
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("cell") as! CommentTableViewCell
        
        cell.comment = comments[indexPath.row]
        
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return comments.count
    }
}


extension CommentsViewController : UITextFieldDelegate {
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}