//
//  APIResource.swift
//  looks-party
//
//  Created by BEPID on 26/07/16.
//  Copyright © 2016 BEPID. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

typealias CompletionBlockMasks = ((Result<[Mask], APIError>) -> ())?
typealias CompletionBlockPublications = ((Result<[Publication], APIError>) -> ())?
typealias CompletionBlockPublication = ((Result<Publication, APIError>) -> ())?
typealias CompletionBlockComments = ((Result<[Comment], APIError>) -> ())?


func api_URL(string : String?) -> NSURL? {
    
    if let stringUrl = string {
        return NSURL(string: API.baseUrl + stringUrl)
    }
    
    return nil
}

extension API {
    
    struct Publications {
        static func get(completionHandler: CompletionBlockPublications) {
            API.requestArray(.GET,
                             url: API.baseUrl + "/publications",
                             parameters: ["filter[order]": "date_created DESC"],
                             encoding: ParameterEncoding.URLEncodedInURL,
                             completionHandler: completionHandler)
        }
        
        static func create(publication : Publication, completionHandler : CompletionBlockPublication) {
            API.requestObject(.POST, url: API.baseUrl + "/publications", parameters: publication.toJSON(), completionHandler: completionHandler)
        }
        
        struct Comments {
            static func get(publicationId : String,completionHandler: CompletionBlockComments) {
                API.requestArray(.GET,
                                 url: API.baseUrl + "/publications/\(publicationId)/comments",
                                 parameters: ["filter[order]": "date_created DESC"],
                                 encoding: ParameterEncoding.URLEncodedInURL,
                                 completionHandler: completionHandler)
            }
            
            static func create(publicationId : String, comment: Comment, completionHandler : CompletionBlockAPI){
                API.requestJSON(.POST, url: API.baseUrl + "/publications/\(publicationId)/comments", parameters: comment.toJSON(), completionHandler: completionHandler)
            }
        }
    }
    
    struct Mask {

        static func list(completionHandler: CompletionBlockMasks) {
            API.requestArray(.GET, url: API.baseUrl + "/masks", parameters: nil, completionHandler: completionHandler)
        }
   
    }
    
    struct Upload {
        static func image(image : UIImage, completionHandler : CompletionBlockAPI) {
            let imageData = UIImageJPEGRepresentation(image, 1) as NSData!
            API.requestUploadJSON(.POST, url: API.baseUrl + "/containers/mask_images/upload", imageData: imageData, completionHandler: completionHandler)
        }
    }
    
    struct Instagram {
//        https://api.instagram.com/v1/tags/{tag-name}/media/recent?access_token=ACCESS-TOKEN
        static func get(completionHandler : CompletionBlockAPI) {
            let token = "f98b6051b39e4d619c4fd383eb20e5bb"
            let url = "https://api.instagram.com/v1/tags/looksparty/media/recent"
            API.requestJSON(.GET, url: url, parameters: ["access_token" : token], encoding: ParameterEncoding.URLEncodedInURL ,completionHandler: completionHandler)
        }
    }
}