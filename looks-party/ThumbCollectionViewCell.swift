//
//  ThumbCollectionViewCell.swift
//  looks-party
//
//  Created by BEPID on 27/07/16.
//  Copyright © 2016 BEPID. All rights reserved.
//

import UIKit

class ThumbCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var thumbImageView: UIImageView!
    
    var mask : Mask? {
        didSet{
            update()
        }
    }
    
    func update() {
        
        if let imageUrl = api_URL(mask?.thumbUrl){
            thumbImageView.af_setImageWithURL(imageUrl)
        }
        
    }
}
