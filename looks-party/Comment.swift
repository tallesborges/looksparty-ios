//
//  Comment.swift
//  looks-party
//
//  Created by BEPID on 27/07/16.
//  Copyright © 2016 BEPID. All rights reserved.
//

import UIKit
import ObjectMapper

class Comment: NSObject,Mappable {

    var id : String?
    var text : String?
    var createdDate : NSDate?
    
    static let dateFormat = CustomDateFormatTransform(formatString: "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    
    convenience required init?(_ map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        text <- map["text"]
        createdDate <- (map["date_created"],Comment.dateFormat)
    }
    
}







